<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapperInt\Repositories;

use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Kaskadia\Lib\DoctrineRepositoryWrapperInt\Entities\Interfaces\IEntity;
use Kaskadia\Lib\DoctrineRepositoryWrapperInt\Repositories\Interfaces\IDoctrineWrapperRepository;

class DoctrineWrapperRepository implements IDoctrineWrapperRepository {
	public function __construct(EntityManagerInterface $entityManager) {
		$this->em = $entityManager;
	}

	protected EntityManagerInterface $em;

	public EntityRepository $repository;

	protected string $entity;

	public function initializeRepository(string $entity): void {
		$this->entity = $entity;
		$metaData = new ClassMetadata($this->entity);
		$this->repository = new EntityRepository($this->em, $metaData);
	}

	public function find(int $id) {
		return $this->repository->find($id);
	}

	public function findAll(): array {
		return $this->repository->findAll();
	}

	public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array {
		return $this->repository->findBy($criteria, $orderBy, $limit, $offset);
	}

	public function findOneBy(array $criteria) {
		return $this->repository->findOneBy($criteria);
	}

	public function save(IEntity $entity): void {
		$this->em->persist($entity);
	}

	public function flush(): void {
		$this->em->flush();
	}

	public function saveAndFlush(IEntity $entity): void {
		$this->save($entity);
		$this->flush();
	}

	public function getQueryBuilder(string $alias, string $indexBy = null): QueryBuilder {
		return $this->repository->createQueryBuilder($alias, $indexBy);
	}

	public function getEntityManager(): EntityManagerInterface {
		return $this->em;
	}
}