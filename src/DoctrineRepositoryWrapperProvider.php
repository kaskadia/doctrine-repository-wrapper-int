<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapperInt;

use Illuminate\Support\ServiceProvider;
use Kaskadia\Lib\DoctrineRepositoryWrapperInt\Repositories\DoctrineWrapperRepository;
use Kaskadia\Lib\DoctrineRepositoryWrapperInt\Repositories\Interfaces\IDoctrineWrapperRepository;

class DoctrineRepositoryWrapperProvider extends ServiceProvider {
	/**
	 * Bootstrap the application services.
	 *
	 * @return void
	 */
	public function boot() {
	}

	/**
	 * Register the application services.
	 *
	 * @return void
	 */
	public function register() {
		$this->app->bind(IDoctrineWrapperRepository::class, DoctrineWrapperRepository::class);
	}
}