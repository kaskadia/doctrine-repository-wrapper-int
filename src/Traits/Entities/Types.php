<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapperInt\Traits\Entities;

use Doctrine\ORM\Mapping as ORM;

trait Types {
	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected string $name;

	/**
	 * @var string
	 * @ORM\Column(type="string")
	 */
	protected string $slug;

	public function getName(): string {
		return $this->name;
	}

	public function setName(string $name): void {
		$this->name = $name;
	}

	public function getSlug(): string {
		return $this->slug;
	}

	public function setSlug(string $slug): void {
		$this->slug = $slug;
	}
}