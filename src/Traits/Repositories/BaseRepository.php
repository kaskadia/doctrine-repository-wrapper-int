<?php /** @noinspection PhpUndefinedFieldInspection */

namespace Kaskadia\Lib\DoctrineRepositoryWrapperInt\Traits\Repositories;

use Kaskadia\Lib\DoctrineRepositoryWrapperInt\Entities\Interfaces\IEntity;
use Kaskadia\Lib\DoctrineRepositoryWrapperInt\Repositories\Interfaces\IDoctrineWrapperRepository;

trait BaseRepository {
	public function findAll(): array {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		return $repo->findAll();
	}

	public function findBy(array $criteria, array $orderBy = null, int $limit = null, int $offset = null): array {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		return $repo->findBy($criteria, $orderBy, $limit, $offset);
	}

	public function save(IEntity $entity): void {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		$repo->save($entity);
	}

	public function flush(): void {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		$repo->flush();
	}

	public function saveAndFlush(IEntity $entity): void {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		$repo->saveAndFlush($entity);
	}

	public function delete(IEntity $entity): void {
		/** @var IDoctrineWrapperRepository $repo */
		$repo = $this->repository;
		$repo->getEntityManager()->remove($entity);
	}

	public function deleteAndFlush(IEntity $entity): void {
		$this->delete($entity);
		$this->flush();
	}
}