<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapperInt\Entities;

use Kaskadia\Lib\DoctrineRepositoryWrapperInt\Entities\Interfaces\IEntity;
use Exception;
use Doctrine\ORM\Mapping as ORM;

abstract class EntityBase implements IEntity {
	/**
	 * EntityBase constructor.
	 * @throws Exception
	 */
	public function __construct() {
		if(!isset($this->id)) {
			/**
			 * PHP 7.4 requires typed properties to be initialized before accessing.
			 * Doctrine won't be able to save the entity without this.
			 */
			$this->id = -1;
		}
	}

	/**
	 * @ORM\Id
	 * @ORM\GeneratedValue(strategy="IDENTITY")
	 * @ORM\Column(type="integer")
	 */
	protected int $id;

	public function getId(): int {
		return $this->id;
	}


	/**
	 * Only sets id if unset. This is primarily for deserialization.
	 * We still want this property to be immutable.
	 * @param int $id
	 */
	public function setId(int $id): void {
		if (!isset($this->id)) {
			$this->id = $id;
		}
	}
}