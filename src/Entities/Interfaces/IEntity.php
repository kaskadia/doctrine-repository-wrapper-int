<?php

namespace Kaskadia\Lib\DoctrineRepositoryWrapperInt\Entities\Interfaces;

interface IEntity {
	public function getId(): int;
	public function setId(int $id): void;
}